
# Text styles
bold=$(tput bold)
white=$(tput setaf 7)
lightgreen=$(tput setaf 92)
green=$(tput setaf 2)
txtreset=$(tput sgr0)

echo "${bold}
         :XWWWd.
         :XMWWd.
dxxd;    .;:;;.       .,cdxOOOkxoc'.
NMMWd              .;l0NMMMWWWWMMMNOc.
NMMWd             ;0WWMMNkc;,,;lkNMMWO,
NMMWd            ;KMMMW0;        :KMMM0,
NMMWd           .kMMMMK;          :XMMWx
NMMWd           ,KMMMMk.          .OMMMK
NMMWd           '0MMMMk.          .OMMM0
NMMWd           .xWMMMX:          cNMMWd
NMMWd            ,0WMMMKl.      .lXMMWO'
NMMWd             'xXNMMW0dlcclxKWMMNd.
NMMWd              ..;xKWMMMMMMMMNKd,
kOOO:                  'lk0XNNX0xc.       ${txtreset}"
echo "${bold}
        intervaro-start
      ${txtreset}"


echo "1) Enter a site-url (Example: sitename.myshopify.com). Press ENTER to skip"
read site_url

echo "2) Enter Theme-ID (Development). Press ENTER to skip"
read development_id

echo "3) Enter Theme-ID (Staging). Press ENTER to skip"
read staging_id

echo "4) Enter Theme-ID (Production). Press ENTER to skip"
read production_id

echo "5) Is following information correct?"

if test -n "$site_url"; then
  echo "Site-url: ${site_url}"
fi

if test -n "$development_id"; then
  echo "Theme-ID (Development): ${development_id}"
fi

if test -n "$staging_id"; then
  echo "Theme-ID (Staging): ${staging_id}"
fi

if test -n "$production_id"; then
  echo "Theme-ID (Production): ${production_id}"
fi

while true; do
read -p "
Proceed to install? [y/N]
" yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer y or n.";;
  esac
done

if test -n "$site_url"; then
  sed -i '' -e "s/SITE_URL/$site_url/g" package.json
fi

if test -n "$development_id"; then
  sed -i '' -e "s/DEVELOPMENT_ID/$development_id/g" package.json
fi

if test -n "$staging_id"; then
  sed -i '' -e "s/STAGING_ID/$staging_id/g" package.json
fi

if test -n "$production_id"; then
  sed -i '' -e "s/PRODUCTION_ID/$production_id/g" package.json
fi

echo "Done"

















