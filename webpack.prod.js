/* global module */

const merge = require('webpack-merge');
const common = require('./webpack.common.js');

// Config files.
const settings = require('./webpack.settings.js');

module.exports = merge(common, {
	mode: 'production'
});

