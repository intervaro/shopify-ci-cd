# Web fonts

We use an online generator to format fonts we get from designers to be cross browser compatible and nice. The online generator we use is Transfonter.

## Instructions

### 1. Create Web Font Package
 * Use: https://transfonter.org/
 * Settings:
 	- Family support ON,
 	- Autohint ON,
 	- Add local() rule ON,
 	- Optional: Fix vertical metrics ON (This can be helpful for cross browser compatibility)
	- Base64 Encode: OFF
 * Formats: WOFF, WOFF2 – For [Practical Level of Browser Support](https://css-tricks.com/snippets/css/using-font-face/#article-header-id-1)


### 2. Place the files
 * Place all font files in /dev/fonts folder.
 * Add @font-face to css/settings/fonts.css
 * That's it!
 * (´npm run watch´ or ´npm run dev´ will move font files in /dev/fonts/ folder automatically)
