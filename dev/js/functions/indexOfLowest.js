
/**
 * Get index of lowest.
 *
 * By inputing an array with objects. It selects the lowest item-value.
 *
 * Example:
 *  [{ id: 1, value: 200 }]
 *  [{ id: 2, value: 400 }]
 *
 *  In this example, the first item will be picked since
 *  it has the lowest value.
 *
 * @param {array} list - The list to select lowest item-value from.
 */
export const indexOfLowest = ( list ) => {

	// Make every number in the array positive.
	list = list.map( ( item ) => {
		item.value = Math.abs( item.value );
		return item;
	} );

	// Sort the items based on its value.
	list.sort( function( a, b ) {
		return a.value - b.value;
	} );

	return list[ 0 ];
};
