/**
 * Foreach
 *
 * @param {func} callback Function to execute on each run.
 */
if ( ! NodeList.prototype.forEach ) {
	NodeList.prototype.forEach = function( callback ) {
		for ( let i = 0; i < this.length; i++ ) {
			callback( this[ i ], i );
		}
	};
}

/**
 * Matches
 */
if ( ! Element.prototype.matches ) {
	Element.prototype.matches =
		Element.prototype.msMatchesSelector ||
		Element.prototype.webkitMatchesSelector;
}

/**
 * Find the closest element.
 *
 * @param {string} s Element to search/look for.
 */
if ( ! Element.prototype.closest ) {
	Element.prototype.closest = function( s ) {
		let el = this;
		if ( ! document.documentElement.contains( el ) ) {
			return null;
		}
		do {
			if ( el.matches( s ) ) {
				return el;
			}
			el = el.parentElement || el.parentNode;
		} while ( el !== null && el.nodeType === 1 );
		return null;
	};
}
