import { _changeState } from './_changeState';

/*
 * Toggle accordion-item state on click.
 */
export const _onClick = () => {
	const elements = document.querySelectorAll( '.js-accordion .js-accordion-button' );

	elements.forEach( ( button ) =>
		button.addEventListener( 'click', () => _changeState( button ) )
	);
};
