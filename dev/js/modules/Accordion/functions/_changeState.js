
/*
 * Change accordion-item state.
 */
export const _changeState = ( button ) => {
	const id = button.id.replace( 'button', 'content' );
	const item = button.closest( '.js-accordion-item' );
	const content = document.querySelector( `#${ id }` );

	item.classList.toggle( 'is-open' );

	if ( item.classList.contains( 'is-open' ) ) {
		button.setAttribute( 'aria-expanded', true );
		content.style.height = `${ content.scrollHeight }px`;
		content.setAttribute( 'aria-hidden', false );
	} else {
		button.setAttribute( 'aria-expanded', true );
		content.style.height = 0;
		content.setAttribute( 'aria-hidden', true );
	}
};
