
/*
 * Change accordion-content height when resizing window.
 */
export const _onResize = () => {
	window.addEventListener( 'resize', () => {
		const elements = document.querySelectorAll( `.js-accordion .js-accordion-item.is-open .js-accordion-content` );

		elements.forEach( ( element ) =>
			element.height = element.scrollHeight
		);
	} );
};
