import { Module } from '../Module';
import { _onClick } from './functions/_onClick';
import { _onResize } from './functions/_onResize';

/*
 * Module for Accordions.
 *
 * This module brings functionality
 * for accordions.
 */
export class Accordion extends Module {
	initialize() {
		_onClick();
		_onResize();
	}
}
