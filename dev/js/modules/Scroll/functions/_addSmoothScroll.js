/* Import polyfill for smoothscroll from npm */
import smoothscroll from 'smoothscroll-polyfill';

/**
 * Add smooth-scroll for older browsers.
 */
export const _addSmoothScroll = () => {
	smoothscroll.polyfill();

	const navHeight = document.getElementById( 'header' ).offsetHeight;
	const buttons = document.querySelectorAll( 'a.js-smooth-scroll[href^="#"]' );

	buttons.forEach( ( button ) =>
		button.addEventListener( 'click', ( e ) => {
			e.preventDefault();

			const href = button.getAttribute( 'href' );
			const element = href != '#' ? document.querySelector( href ) : null;

			if ( element !== null ) {
				const bodyTop = document.body.getBoundingClientRect().top;
				const elementTop = element.getBoundingClientRect().top;
				const scrollTo = elementTop - navHeight - bodyTop;

				window.scrollTo( { top: scrollTo, left: 0, behavior: 'smooth' } );
			}
		} )
	);
};
