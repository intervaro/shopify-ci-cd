/**
 * Check if element is or was in view.
 */
export const _inView = () => {
	const scrollSpy = document.querySelectorAll( '.js-scrollspy' );

	if ( 'IntersectionObserver' in window ) {
		const observer = new IntersectionObserver( observerOnChange );

		scrollSpy.forEach( ( element ) => {
			observer.observe( element );
		} );
	} else {
		/* Trigger all scrollspies on page load for browsers
		not supporting IntersectionObserver */
		scrollSpy.forEach( ( element ) => {
			isInView( element );
			wasInView( element );
		} );
	}
};

/**
 * Add appropriate classes of element 'is' in view.
 *
 * @param {node} element Element to check.
 */
export const isInView = ( element ) => {
	const hasClass = element.classList.contains( 'is-in-view' );

	if ( ! hasClass ) {
		element.classList.add( 'is-in-view' );
	}
};

/**
 * Add appropriate classes of element 'is not' in view.
 *
 * @param {node} element Element to check.
 */
export const isNotInView = ( element ) => {
	const hasClass = element.classList.contains( 'is-in-view' );

	if ( hasClass ) {
		element.classList.remove( 'is-in-view' );
	}
};

/**
 * Add appropriate classes of element 'was' in view.
 *
 * @param {node} element Element to check.
 */
export const wasInView = ( element ) => {
	const hasClass = element.classList.contains( 'was-in-view' );

	if ( ! hasClass ) {
		element.classList.add( 'was-in-view' );
	}
};

/**
 * InterSectionObserver onChange function
 *
 * @param {NodeList} entries Elements to check.
 * @param {object} observer
 */
export const observerOnChange = ( entries, observer ) => {
	entries.forEach( function( entry ) {
		if ( entry.intersectionRatio > 0 ) {
			isInView( entry.target );
			wasInView( entry.target );
		} else {
			isNotInView( entry.target );
		}
	} );
};
