import { Module } from '../Module';
import { _addSmoothScroll } from './functions/_addSmoothScroll';
import { _inView } from './functions/_inView';

/**
 * Module for scroll.
 *
 * This module runs all neccassary functions
 * for a better scroll-interactivity.
 */
export class Scroll extends Module {
	initialize() {
		_addSmoothScroll();
		_inView();
	}
}
