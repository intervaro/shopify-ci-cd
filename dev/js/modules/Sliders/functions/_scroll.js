import { _elements } from './_elements';

/*
 * This function is used to scroll to a certain item in the slider.
 * In most cases this will be used to scroll to the next/previous item.
 */
export const _scroll = ( slider, item ) => {
	const {
		list,
	} = _elements( slider );

	const padding = parseInt( window.getComputedStyle( list ).getPropertyValue( 'padding-left' ) );
	const scrollToLeft = item.getBoundingClientRect().left - padding - list.getBoundingClientRect().left;

	list.scrollTo( {
		left: list.scrollLeft + scrollToLeft,
		behavior: 'smooth',
	} );
};
