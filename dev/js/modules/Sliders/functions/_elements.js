
/*
 * This function is used to gather all of the slider-elements
 * and return them into an object.
 */
export const _elements = ( slider ) => {
	if ( slider ) {
		const sliderId = slider.id;

		// Slider
		const list = slider.querySelector( '.js-slider-list' );
		const items = list.querySelectorAll( '.js-slider-item' );
		const current = slider.getAttribute( 'data-slider-current' ) ? parseInt( slider.getAttribute( 'data-slider-current' ) ) : 0;

		// Navigation
		const navigation = sliderId ? document.querySelector( `.js-navigation[data-target="${ sliderId }"]` ) : null;
		const prev = navigation ? navigation.querySelector( '.js-navigation-prev' ) : slider.querySelector( '.js-slider-prev' );
		const next = navigation ? navigation.querySelector( '.js-navigation-next' ) : slider.querySelector( '.js-slider-next' );

		return {
			slider,
			list,
			prev,
			next,
			items,
			current,
		};
	}

	return false;
};
