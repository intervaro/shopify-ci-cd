import { _elements } from './_elements';
import { _scroll } from './_scroll';

/*
 * This function navigates to the next-item.
 */
export const _next = ( slider ) => {
	const {
		current,
		items,
	} = _elements( slider );

	const next = items[ current + 1 ];

	if ( next ) {
		_scroll( slider, next );
	}
};
