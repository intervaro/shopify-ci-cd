import { indexOfLowest } from '../../../functions/indexOfLowest';

/*
 * This function checks which of the slider-items
 * is the closest to the viewport.
 */
export const _point = ( slider ) => {
	const points = [ ...slider.querySelectorAll( '.js-slider-item' ) ];
	const positions = points.map( ( pointer, i ) => ( { id: i, value: pointer.getBoundingClientRect().left - slider.getBoundingClientRect().left } ) );

	return indexOfLowest( positions );
};
