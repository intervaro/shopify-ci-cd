import { _elements } from './_elements';
import { _scroll } from './_scroll';

/*
 * This function navigates to the previous-item.
 */
export const _prev = ( slider ) => {
	const {
		current,
		items,
	} = _elements( slider );

	const prev = items[ current - 1 ];

	if ( prev ) {
		_scroll( slider, prev );
	}
};
