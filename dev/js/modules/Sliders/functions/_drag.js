import { _elements } from './_elements';
// import { _scroll } from './_scroll';

/*
 * This function adds 'draggable'-functionality to sliders.
 */
export const _drag = ( slider = null ) => {
	const {
		list,
	} = _elements( slider );

	const position = {
		left: 0,
		x: 0,
	};

	const onDragStart = ( e ) => {
		position.left = list.scrollLeft;
		position.x = e.clientX;

		list.classList.add( 'is-dragging' );

		list.addEventListener( 'mousemove', onDragMove );
		list.addEventListener( 'mouseup', onDragEnd );
	};

	const onDragMove = ( e ) => {
		const dx = e.clientX - position.x;
		list.scrollLeft = position.left - dx;
	};

	const onDragEnd = () => {
		list.classList.remove( 'is-dragging' );
		list.removeEventListener( 'mousemove', onDragMove );
		list.removeEventListener( 'mouseup', onDragEnd );
	};

	list.addEventListener( 'mousedown', onDragStart );
};
