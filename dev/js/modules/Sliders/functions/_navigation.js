import { _drag } from './_drag';
import { _next } from './_next';
import { _prev } from './_prev';
import { _point } from './_point';
import { _elements } from './_elements';

/*
 * Add navigation-functionality to every
 * slider using the data-attribute: slider ([data-slider]).
 */
export const _navigation = () => {
	const sliders = document.querySelectorAll( '.js-slider' );

	sliders.forEach( ( slider ) => {
		const isDraggable = slider.classList.contains( 'js-slider-draggable' );

		const {
			list,
			prev,
			next,
			items,
		} = _elements( slider );

		if ( ! list || items.length < 1 ) {
			return;
		}

		if ( isDraggable ) {
			_drag( slider );
		}

		if ( prev && next ) {
			prev.addEventListener( 'click', () => _prev( slider ) );
			next.addEventListener( 'click', () => _next( slider ) );
		}

		if ( list ) {
			list.addEventListener( 'scroll', () => {
				const current = _point( list )?.id;
				slider.setAttribute( 'data-slider-current', current );
			} );
		}
	} );
};

