import { Module } from '../Module';
import { _navigation } from './functions/_navigation';
// import { _slick } from './functions/_slick';

/**
 * Module for Sliders.
 *
 * This module brings navigation-functionality
 * to slider-blocks & components.
 */
export class Sliders extends Module {
	initialize() {
		_navigation();
		// _slick();
	}
}
