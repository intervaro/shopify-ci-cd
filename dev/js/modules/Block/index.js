import { Intervaro } from '../Intervaro/';

/**
 * Module for blocks.
 *
 * This module gets attached to each block and allows
 * custom functions to get executued on block-load, both front-end and back-end.
 *
 * @param {object} props Module properties.
 */
export class Block {
	constructor( props = {} ) {
		this.props = { ...props };
		this.initialize();
	}

	initialize() {
		this.loaded();
		this.editor();
	}

	/*
	 * Add block-status to 'window.blocks'.
	 */
	loaded() {
		if ( ! window.blocks ) {
			window.blocks = {};
		}
	}

	/*
	 * Run code for block in editor.
	 */
	editor() {
		if ( window.acf ) {
			window.acf.addAction(
				`render_block_preview/type=${ this.props.type }`, ( block ) => {
					const min = 10000000000000000;
					const max = 99999999999999999;
					const id = Math.floor( Math.random() * ( max - min + 1 ) ) + min;

					// Update block status to loaded.
					window.blocks[ `${ this.props.type }-${ id }` ] = true;

					// Run initialize callback.
					// this.props.initialize( block );
				}
			);
		}
	}
}
