/**
 * Module
 *
 * This is a parent class that can be used by modules to get global access.
 * Global properties and methods can be accessed by using super();
 *
 * By standard all modules take in a single paramteter, "props" (External-properties).
 * These properties can then be merged into "this.props" (Local-properties).
 *
 * Each module has its own separate folder, consisting of an index file paired with a functions-folder.
 *
 * Module/
 * -- index.js   ( Main file for the module. )
 * -- functions/ ( All partial-functions for the module. )
 * ---- _function1.js
 * ---- _function2.js
 * ---- _function3.js
 *
 * A module should look like the example below, with a very light structure.
 * Most of the code should be written into functions, which will make it more maintainable.
 *
 * Example:
 *
 * import { Module } from '../Module';
 * import { _function } from './functions/_cookieNotice';
 * import { _setCookie } from './functions/_setCookie';
 *
 * # -- # modules/Module.js # -- # ---------------------------------------------------------------------------- # -- #
 *
 * class Module {
 *
 *   constructor() {
 *     this.prefix = 'intervaro';
 *   }
 *
 *   message( msg ) {
 *     console.log( msg );
 *   }
 *
 * }
 *
 * # -- # modules/Cookies/index.js # -- # --------------------------------------------------------------------- # -- #
 *
 * import { _setCookie } from './functions/_setCookie';
 * import { _cookieNotice } from './functions/_cookieNotice';
 *
 * class Cookies extends Module {
 *
 *   constructor( props = {} ) {
 *
 * 		 // Inherit global-properties and methods.
 * 	   super();
 *
 * 		 // Merge global, external and local-properties.
 *  	 this.props = { ...props, ...this.props };
 *
 * 		 // Initialize module functions.
 * 		 this.initialize();
 * 	 }
 *
 * 	 /**
 *    * An 'initialize' method executes all necessary
 *    * functions on module construct.
 *    *_/
 * 	 initialize() {
 *     _cookieNotice();
 * 	 }
 *
 * 	 /**
 *    * Other methods can be added to execute
 *    * code after module initialization.
 *    *_/
 * 	 setCookie( name, value, days ) {
 *
 *     // Add prefix to name.
 *     name = `${ this.props.prefix }_${ name }`;
 *
 *     // Set cookie.
 *     _setCookie( `${ this.props.prefix }_${ name }`, value, days );
 *
 * 		 // Output to console that a cookie has been set.
 *     this.message( `Cookie has been set: ${ name }` );
 *
 *   }
 *
 * }
 *
 * # -- # script.js # -- # ------------------------------------------------------------------------------------ # -- #
 *
 * import { Cookies } from './modules/Cookies/';
 *
 * Intervaro.load( () => {
 *
 *   // All modules gets constructed in 'modules' so that they later can be accessed.
 *   const modules = {
 *	   cookies: new Cookies(),
 *   };
 *
 *   // Accessing module after initialization.
 *   modules.cookies.setCookie();
 *
 * } );
 *
 */
export class Module {

	constructor( props = {} ) {

		// Setup properties.
		this.props = props;

		// Run initialization.
		if ( this.initialize ) {
			this.initialize();
		}
	}

	/**
	 * = Get Props =
	 * This function allows you to get the prop data.
	 *
	 * @param {*} get - Get argument.
	 */
	get( get = null ) {

		if ( get === null ) {
			return this.props;
		} else if ( typeof get === 'string' ) {
			return this.props[ get ];
		} else if ( typeof get === 'object' ) {
			return { ...get, ...this.props };
		}

	}

	/**
	 * = Set Props =
	 * This function allows you to set the prop data.
	 *
	 * @param {object} update - New values to update props with.
	 */
	set( update = {} ) {
		this.props = {
			...this.props,
			...update,
		};

		// Run update-function after props has been updated.
		if ( this.update ) {
			this.update();
		}
	}

}
