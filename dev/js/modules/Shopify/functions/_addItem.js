
/**
 * Add product-item to the cart with Shopify API.
 */
export const _addItem = (variantID = null, quantity = 1, properties = null) => {

	if (variantID) {

		const data = {
			items: [{
				id: variantID,
				quantity: quantity ? parseInt(quantity) : 1,
			}]
		};

		if (properties) {
			data.items[0].properties = properties;
		}

		return fetch('/cart/add.js', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(data)
		})
		.then(res => res.json ? res.json() : res)
		.catch(err => console.log('Error', err));
	}

	return false;
}
