
/**
 * Get the cart with Shopify API.
 */
export const _getCart = (variantID, options) => {
	return fetch('/cart.js')
		.then(res => res.json ? res.json() : res)
		.catch(err => console.log('Error', err))
}
