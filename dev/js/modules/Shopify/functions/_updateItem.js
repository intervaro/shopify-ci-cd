
/**
 * Update item-quantity in the cart with Shopify API.
 */
export const _updateItem = (lineItemKey = '', quantity = 1) => {

	if (lineItemKey) {

		const data = {
			updates: {
				[lineItemKey]: quantity
			}
		};

		return fetch('/cart/update.js', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(data)
		})
		.then(res => res.json ? res.json() : res)
		.catch(err => console.log('Error', err));
	}

	return false;
}
