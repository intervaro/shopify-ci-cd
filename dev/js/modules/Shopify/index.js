import { Module } from '../Module';
import { _formatMoney } from './functions/_formatMoney';

/**
 * Module for Shopify-functions.
 *
 * This module contains Shopify-functions.
 */
export class Shopify extends Module {
	initialize() {
		window.Shopify = window.Shopify || {};
		window.Shopify.formatMoney = window.Shopify.formatMoney || _formatMoney;
	}
}
