import { Module } from '../Module';
import { _outline } from './functions/_outline';

/**
 * Module for WCAG 2.1 compliance.
 *
 * This module generates required markup and
 * functions for WCAG 2.1 compliance.
 */
export class Wcag extends Module {
	initialize() {
		_outline();
	}
}
