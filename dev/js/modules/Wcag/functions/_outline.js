
/**
 * Adds an outline to elements only when using keyboard.
 */
export const _outline = () => {
	isUsingMouse();
	isUsingKeyboard();
};

/*
 * Activates when using mouse.
 */
const isUsingMouse = ( body = document.body ) => {
	body.classList.add( 'using-mouse' );

	body.addEventListener( 'mousedown', () => {
		body.classList.remove( 'using-keyboard' );
		body.classList.add( 'using-mouse' );
	} );
};

/*
 * Activates when using keyboard.
 */
const isUsingKeyboard = ( body = document.body ) => {
	const KEYCODE_TAB = 9;

	body.addEventListener( 'keydown', ( e ) => {
		if ( e.keyCode === KEYCODE_TAB ) {
			body.classList.remove( 'using-mouse' );
			body.classList.remove( 'using-keyboard' );
		}
	} );
};
