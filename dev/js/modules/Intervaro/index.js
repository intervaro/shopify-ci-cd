import { _message } from './functions/_message';
import { _blocks } from './functions/_blocks';

/**
 * Intervaro class.
 *
 * This class acts as container for all of the JavaScript.
 * For an example you can wrap events to make them not
 * only more aesthetically pleasing, but more functional.
 *
 * static update( callback ) {
 *   setInterval( callback, 250 );
 * }
 *
 * Intervaro.update( () => {
 *   console.log( 'Update' );
 * } );
 *
 */
export class Intervaro {
	/**
	 * Runs when document has loaded.
	 *
	 * @param {func}   callback Function to run on load.
	 * @param {number} delay    The amount of time before the functions loads.
	 */
	static load( callback, delay = 0 ) {
		window.addEventListener( 'load', () =>
			setTimeout( () => {
				document.body.classList.add( 'has-loaded' );

				_message( '#000000', '#FFFFFF' );
				callback();
			}, delay )
		);
	}

	/**
	 * Runs when content has-loaded.
	 *
	 * @param {func}   callback Function to run when ready.
	 * @param {number} delay    The amount of time before the functions loads.
	 */
	static ready( callback, delay = 0 ) {
		window.addEventListener( 'DOMContentLoaded', () =>
			setTimeout( () => {
				document.body.classList.add( 'is-ready' );

				callback();
			}, delay )
		);
	}

	/**
	 * Runs when all blocks has been loaded (Editor).
	 *
	 * @param {func} callback
	 */
	static blocks( callback ) {
		if ( window.acf ) {
			// Add block-load event every time html updates.
			window.acf.addAction( 'append', () => {
				const interval = setInterval( () => {
					if ( _blocks() ) {
						callback();
						clearInterval( interval );
					}
				}, 1 );
			} );
		}
	}
}
