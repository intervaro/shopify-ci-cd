/**
 * This method outputs a 'developer-message' in the console.
 *
 * @param {string} background Message background-color.
 * @param {string} color      Message text-color.
 */
export const _message = ( background, color ) => {
	console.log(
		'%c___Developed by Intervaro – Oh my!_____________\n\n' +
			'___Are you interested in our code?_____________\n\n' +
			'___We are always looking for awesome coders!___\n\n' +
			'___Contact us to come work with us!____________',
		`background: ${ background }; color: ${ color }`
	);

	console.log('hello im here');
};
