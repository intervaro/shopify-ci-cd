
/**
 * Checks if all blocks has been loaded.
 */
export const _blocks = () => {
	const allHasBeenLoaded = Object.keys( window.blocks ).find(
		( block ) => window.blocks[ block ] === false
	);

	// Return true if all blocks has been loaded.
	if ( allHasBeenLoaded === undefined ) {
		return true;
	}

	return false;
};
