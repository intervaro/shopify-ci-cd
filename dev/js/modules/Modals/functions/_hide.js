/**
 * This method hides the target-modal.
 *
 * @param {node} target Target to update.
 */
export const _hide = function( target ) {
	if ( target !== null ) {
		const targetID = target.getAttribute( 'id' );

		const buttons = document.querySelectorAll( '.js-modal-btn[data-target="' + targetID + '"]' );

		target.classList.add( 'is-animating-out' );

		const listener = function() {
			target.classList.remove( 'is-visible' );
			target.classList.remove( 'is-animating-out' );
			target.setAttribute( 'aria-hidden', 'true' );

			buttons.forEach( ( button ) => {
				button.classList.remove( 'is-active' );
				button.setAttribute( 'aria-expanded', 'false' );
			} );

			target.removeEventListener( 'animationend', listener );
		};

		target.addEventListener( 'animationend', listener );

		document.body.classList.remove( 'has-modal' );
	}
};
