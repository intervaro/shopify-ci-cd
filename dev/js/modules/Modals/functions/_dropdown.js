
export const _dropdown = () => {
	const buttons = document.querySelectorAll( '.c-modal .dropdown' );

	/**
	 * Add button-event
	 */
	buttons.forEach( ( button ) =>
		button.addEventListener( 'click', () => {
			if ( ! button.classList.contains( 'has-dropped' ) ) {
				// Remove 'has-dropped' state from every-button.
				buttons.forEach( ( button ) => {
					button.classList.remove( 'has-dropped' );
					button.classList.remove( 'is-active' );
				} );
			}

			// Add 'has-dropped' state to new button.
			button.classList.toggle( 'has-dropped' );
		} )
	);
};
