/**
 * Add a new key-event on 'tab' in modal to make sure focus
 * gets locked withing the dialog.
 *
 * @param {node} first first element in the modal.
 * @param {node} last Last element in the modal.
 */
export const _onTab = ( first, last ) => {
	window.addEventListener( 'keydown', ( e ) =>
		lockTabFocus( e, first, last )
	);
};

const lockTabFocus = ( e, first, last ) => {
	const KEYCODE_TAB = 9;
	const current = document.activeElement;

	if ( e.shiftKey && e.keyCode === KEYCODE_TAB ) {
		// If first element is on focus, set to last.
		if ( current === first ) {
			e.preventDefault();
			last.focus();
		}
	} else if ( e.keyCode === KEYCODE_TAB ) {
		// If last element is on focus, set to first.
		if ( current === last ) {
			e.preventDefault();
			first.focus();
		}
	}
};
