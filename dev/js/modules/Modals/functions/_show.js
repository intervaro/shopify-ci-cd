/**
 * This method shows the target-modal.
 *
 * @param {node} target Target to update.
 */
export const _show = ( target ) => {
	if ( target !== null ) {
		const targetID = target.getAttribute( 'id' );

		const buttons = document.querySelectorAll( '.js-modal-btn[data-target="' + targetID + '"]' );

		target.classList.add( 'is-animating-in' );

		const listener = function() {
			target.classList.add( 'is-visible' );
			target.classList.remove( 'is-animating-in' );
			target.setAttribute( 'aria-hidden', 'false' );

			buttons.forEach( ( button ) => {
				button.classList.add( 'is-active' );
				button.setAttribute( 'aria-expanded', 'true' );
			} );

			target.removeEventListener( 'animationend', listener );
		};

		target.addEventListener( 'animationend', listener );

		document.body.classList.add( 'has-modal' );
	}
};
