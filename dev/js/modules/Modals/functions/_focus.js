import { _onTab } from './_onTab.js';

/**
 * On focus event for modal focusable items.
 *
 * @param {node} target Target to focus.
 */
export const _onFocus = ( target ) => {
	const focusable =
		'a[href], area[href], input:not([disabled]), ' +
		'select:not([disabled]), textarea:not([disabled]),' +
		'button:not([disabled]), [tabindex="0"]';
	const elements = target.querySelectorAll( focusable );
	const first = elements[ 0 ];
	const last = elements[ elements.length - 1 ];

	// Add tab event
	document.addEventListener( 'keydown', () =>
		_onTab( first, last )
	);

	// Set focus to first item in modal.
	setTimeout( () =>
		first.focus()
	, 200 );
};

/**
 * Remove focus event for modal focusable items.
 *
 * @param {nodelist} elementBeforeOpen Element that was active before modal opening.
 */
export const _unFocus = ( elementBeforeOpen ) => {
	// Remove tab event.
	document.removeEventListener( 'keydown', _onTab );

	// Set focus to last item before modal opening.
	setTimeout( () =>
		elementBeforeOpen.focus()
	, 200 );
};
