import { _onTab } from './_onTab.js';

/**
 * On focus event for modal focusable items.
 *
 * @param {nodes} target Target to focus.
 */
export const _onFocus = ( target ) => {
	const focusable =
		'a[href], area[href], input:not([disabled]), ' +
		'select:not([disabled]), textarea:not([disabled]),' +
		'button:not([disabled]), [tabindex="0"]';
	const elements = target.querySelectorAll( focusable );
	const first = elements[ 0 ];
	const last = elements[ elements.length - 1 ];

	// Add tab event
	document.addEventListener( 'keydown', () => _onTab( first, last ) );

	// Set focus to first item in modal.
	setTimeout( () => first.focus(), 200 );
};
