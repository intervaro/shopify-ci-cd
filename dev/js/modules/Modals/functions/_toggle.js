import { _show } from './_show';
import { _hide } from './_hide';
import { _onFocus, _unFocus } from './_focus';

/**
 * Toggle the state for target-modals.
 *
 * @param {nodelist} buttons Buttons/targets to affect.
 */
export const _toggle = () => {
	const buttons = document.querySelectorAll( '.js-modal-btn' );
	let elementBeforeOpen = document.body;

	buttons.forEach( ( button ) =>
		button.addEventListener( 'click', () => {
			// Get target modal.
			const target = document.querySelectorAll(
				button.getAttribute( 'data-target' )
			)[ 0 ];

			// Check if 'elementBeforeOpen' should be updated.
			if ( ! target.classList.contains( 'is-visible' ) ) {
				elementBeforeOpen = document.activeElement;
			}

			setTargetVisibility( target, elementBeforeOpen );
		} )
	);
};

/**
 * Set target visibility.
 *
 * @param {node} target Target to update.
 * @param {node} elementBeforeOpen Element that was active before target was opened.
 */
const setTargetVisibility = ( target, elementBeforeOpen ) => {
	if ( ! target.classList.contains( 'is-visible' ) ) {
		_show( target );
		_onFocus( target );
	} else {
		_hide( target );
		_unFocus( elementBeforeOpen );
	}
};
