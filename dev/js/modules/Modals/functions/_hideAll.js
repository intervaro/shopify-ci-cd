import { _hide } from './_hide.js';

/*
 * Hide all modals when click outside or pressing esc.
 */
export const _hideAll = () => {
	window.addEventListener( 'click', ( e ) => onClick( e ) );
	window.addEventListener( 'keydown', ( e ) => onKey( e ) );
};

/*
 * Close modal when clicking outside.
 */
const onClick = ( e ) => {
	const target = e.target.closest( '.js-modal' );
	const button = e.target.closest( '.js-modal-btn' );

	if ( target === null && button === null ) {
		hideAllOpen();
	}
};

/*
 * Close modal when pressing 'esc'.s
 */
const onKey = ( e ) => {
	if ( e.key === 'Escape' ) {
		hideAllOpen();
	}
};

/*
 * Close all open modals
 */
const hideAllOpen = () => {
	const modals = document.querySelectorAll( '.js-modal.is-visible' );

	if ( modals ) {
		modals.forEach( ( modal ) => {
			_hide( modal );
		} );
	}
};
