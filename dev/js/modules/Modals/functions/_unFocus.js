import { _onTab } from './_onTab';

/**
 * Remove focus event for modal focusable items.
 *
 * @param {nodelist} element
 */
export const _unFocus = ( element ) => {
	// Remove tab event.
	document.removeEventListener( 'keydown', _onTab );

	// Set focus to last item before modal opening.
	setTimeout( () => element.focus(), 200 );
};
