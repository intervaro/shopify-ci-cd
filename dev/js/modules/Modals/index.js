import { Module } from '../Module';
import { _toggle } from './functions/_toggle';
import { _hideAll } from './functions/_hideAll';
import { _dropdown } from './functions/_dropdown';

/**
 * Module for modals.
 *
 * This module adds all functionality
 * for modals.
 */
export class Modals extends Module {
	initialize() {
		_toggle();
		_hideAll();
		_dropdown();
	}
}
