import { Module } from '../Module';
import { _setCookie } from './functions/_setCookie';
import { _cookieNotice } from './functions/_cookieNotice';

/**
 * Module for cookies.
 *
 * This module adds a cookie-notice and has
 * the ability to set custom cookies.
 *
 * @todo Add a delete/remove cookie method.
 */
export class Cookies extends Module {
	initialize() {
		_cookieNotice( 'accept_cookies' );
	}

	set( name, value, days ) {
		_setCookie( name, value, days );
	}
}
