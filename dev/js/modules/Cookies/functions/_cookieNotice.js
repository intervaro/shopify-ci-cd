import { _setCookie } from './_setCookie';

/**
 * Add cookie-notice functionality.
 *
 * @param {string} name Cookie-notice name.
 */
export const _cookieNotice = ( name ) => {
	if ( document.cookie.indexOf( name ) === -1 ) {
		showCookieNotice( name );
	}
};

/**
 * Add/show cookie-notice.
 *
 * @param {string} name Cookie-notice name.
 */
const showCookieNotice = ( name ) => {
	const notice = document.querySelector( '.js-cookie-notice' );
	notice.classList.add( 'is-visible' );

	const button = document.querySelector( '.js-cookie-notice-close' );
	button.addEventListener( 'click', () => hideCookieNotice( name ) );
};

/**
 * Remove/hide cookie-notice.
 *
 * @param {string} name Cookie-notice name.
 */
const hideCookieNotice = ( name ) => {
	_setCookie( name, 1, 365 );

	const notice = document.querySelector( '.js-cookie-notice' );
	notice.classList.remove( 'is-visible' );
};
