/**
 * Set a cookie.
 *
 * @param {string} name  Cookie name.
 * @param {string} value Cookie value.
 * @param {string} days  How many days the cookie will last.
 */
export const _setCookie = ( name, value, days ) => {
	if ( document.cookie.indexOf( 'accept_cookies' ) === -1 ) {
		const date = new Date();
		const DAY_IN_MILLISECONDS = 86400000;
		date.setTime( date.getTime() + ( days * DAY_IN_MILLISECONDS ) );
		const expires = 'expires=' + date.toGMTString();
		document.cookie = name + '=' + value + '; ' + expires + ';';
	}
};
