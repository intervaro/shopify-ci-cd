import { Module } from '../Module';
import { _dynamic } from './functions/_dynamic';
import { _static } from './functions/_static';

/**
 * Module for LazyLoad.
 *
 * This module allows for lazyload use,
 * both "dynamically and statically".
 */
export class LazyLoad extends Module {

	initialize() {

		const functions = {
			dynamic: _dynamic,
			static: _static,
		};

		functions[
			this.get( 'type' ) ? this.get( 'type' ) : 'dynamic'
		]();

	}

}
