
/**
 * Show images when scrolling.
 */
export const _dynamic = () => {
	// Find all images to lazy load
	const images = document.querySelectorAll( 'img[loading="lazy"]' );

	// Keep a count of loaded images for Intersection Observer
	let loaded = 0;
	let observer = null;

	// Trigger an image to load with IntersectionObserver
	const lazyLoad = function( image ) {
		// Ignore if already loaded (or non-lazy)
		if ( image.dataset.loaded !== 'false' ) {
			return;
		}

		// Replace the loading src with the real thing
		image.src = image.dataset.src;
		image.srcset = image.dataset.srcset;
		image.dataset.loaded = true;
		image.classList.add( 'is-loaded' );

		if ( ! observer ) {
			// Return for native lazyloading and older browsers
			return;
		}

		// Unobserve the image and disconnect the Observer
		// if all images have now loaded
		observer.unobserve( image );
		if ( ++loaded >= images.length ) {
			observer.disconnect();
		}
	};

	// Check if the browser support native lazyloading
	if ( 'loading' in HTMLImageElement.prototype ) {
		images.forEach( function( image ) {
			lazyLoad( image );
		} );
	} else if ( 'IntersectionObserver' in window ) {
		// Setup the Observer
		observer = new IntersectionObserver(
			function( entries ) {
				entries.forEach( function( entry ) {
					// Trigger the image to load if within view
					if ( entry.intersectionRatio > 0 ) {
						// `requestAnimationFrame` is not necessary
						// but it (probably) doesn't hurt?
						requestAnimationFrame( function() {
							lazyLoad( entry.target );
						} );
					}
				} );
			},
			{
				rootMargin: '300px 0px',
				threshold: 0.01,
			}
		);
		// Start observing the lazy load images
		images.forEach( function( image ) {
			observer.observe( image );
		} );
	} else {
		// If native lazyloading and Intersection Observer is unsupported
		// load all images immediately
		images.forEach( function( image ) {
			lazyLoad( image );
		} );
	}
};
