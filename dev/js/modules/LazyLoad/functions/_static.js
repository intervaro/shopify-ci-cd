
/**
 * Show all images when blocks has been loaded.
 */
export const _static = () => {
	const images = document.querySelectorAll(
		'.h-lazyload[loading="lazy"]'
	);

	images.forEach( ( image ) => {
		if (
			image.getAttribute( 'src' ) === '' &&
				image.getAttribute( 'srcset' ) === ''
		) {
			image.src = image.dataset.src;
			image.srcset = image.dataset.srcset;
			image.dataset.loaded = true;
			image.classList.add( 'is-loaded' );
		}
	} );
};
