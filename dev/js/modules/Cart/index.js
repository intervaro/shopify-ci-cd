import { Module } from '../Module';
import { _updateCartHTML } from './functions/_updateCartHTML';
import { _getCart } from '../Shopify/functions/_getCart';

/**
 * Module for Cart.
 *
 */
export class Cart extends Module {
	initialize() {
		_getCart()
			.then(cart => {
				_updateCartHTML(cart);
			})
	}
}
