import { _getCartItemHTML } from './_getCartItemHTML';
import { _updateTotalPrice } from './_updateTotalPrice';
import { _updateCartCount } from './_updateCartCount';
import { _onUpdateItem } from './_onUpdateItem';
import { _onRemoveItem } from './_onRemoveItem';

export const _updateCartHTML = (cart) => {

	if (cart) {

		// Get all of them elements.
		const elements = {
			cartForm: document.querySelector('.js-cart-form'),
			cart: document.querySelector('.js-cart'),
			cartEmpty: document.querySelector('.js-cart-empty'),
			cartTotal: document.querySelector('.js-cart-total')
		};

		// elements.cartForm.classList.add('is-loading');

		// Display/Hide cart-empty or cart-form depending on items in cart.
		if (cart.item_count > 0) {
			elements.cartForm.classList.remove('is-hidden');
			elements.cartEmpty.classList.add('is-hidden');
		} else {
			elements.cartForm.classList.add('is-hidden');
			elements.cartEmpty.classList.remove('is-hidden');
		}

		// Clear the current cart-elements.
		elements.cart.innerHTML = '';

		cart.items.forEach((item) => {

			// Create a new item-element and add it to the cart.
			const newItemElement = _getCartItemHTML(item);
			elements.cart.appendChild(newItemElement);

			// Add onChange-event to the quantity-selector.
			newItemElement.querySelector('.js-quantity-selector')
				.addEventListener('change', _onUpdateItem);

			// Add onChange-event to the quantity-selector.
			newItemElement.querySelector('.js-cart-item-remove')
				.addEventListener('click', _onRemoveItem);
		});

		// Update the cart-total price.
		if (elements.cartTotal) {
			elements.cartTotal.innerHTML = Shopify.formatMoney(cart.total_price, theme.moneyFormat);
		}

		_updateCartCount(cart.item_count);
	}
}
