
/**
 * A function that creates the cart-item html.
 */
export const _getCartItemHTML = (item) => {

	const {
		Shopify,
		theme
 	} = window;

	const output = {
		price: '',
		variant: '',
	};

	// Set the variant-title.
	if (item.product_has_only_default_variant === false) {
		output.variant = '<div class="c-cart-item__subtitle">' + item.variant_title + '</div>';
	}

	// Set the price to display.
	if (item.final_price > item.discounted_price) {
		output.price = '<s>' + Shopify.formatMoney(item.final_price, theme.moneyFormat) + '</s><span class="is-on-sale">' + formatMoney(item.discounted_price, theme.moneyFormat) + '</span>';
	} else if (item.compare_at_price > item.final_price) {
		output.price = '<s>' + Shopify.formatMoney(item.compare_at_price, theme.moneyFormat) + '</s><span class="is-on-sale">' + formatMoney(item.final_price, theme.moneyFormat) + '</span>';
	} else {
		output.price = Shopify.formatMoney(item.final_price, theme.moneyFormat)
	}

	// Create item-element and all of the necessary attributes.
	const newItemElement = document.createElement('div');
	newItemElement.classList.add('c-cart-item', 'js-cart-item');
	newItemElement.dataset.lineItemKey = item.key;

	newItemElement.innerHTML = `
		<div class="c-cart-item__figure">
			<figure class="c-cart-item__image">
				<img src="${ item.image }" />
			</figure>
		</div>

		<div class="c-cart-item__content">

			<div class="c-cart-item__header">
				<a class="c-cart-item__title" href="${ item.url }">${ item.title }</a>
				<p class="c-cart-item__price">${ output.price }</p>
			</div>

			<div class="c-quantity-selector">
				<label for="product-qty-${ item.id }" class="c-quantity-selector__label">${ theme.strings.quantity }: </label>
				<select class="c-quantity-selector__select js-quantity-selector" id="product-qty-${ item.id }">
					${
						Array(10).join(0).split(0).map((number, i) => i + 1 === item.quantity ?
						`<option value="${ i + 1 }" selected="selected">${ i + 1 }</option>` :
						`<option value="${ i + 1 }">${ i + 1 }</option>`
						).join('')
					}
				</select>
			</div>

			<div class="c-cart-item__footer">
				<button class="c-cart-item__remove js-cart-item-remove" type="button">${ theme.strings.remove }</button>
			</div>

		</div>
	`;

	return newItemElement;
}
