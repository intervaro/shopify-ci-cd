import { _updateCartHTML } from './_updateCartHTML';
import { _updateItem } from '../../Shopify/functions/_updateItem';

export const _onUpdateItem = (e) => {

	// Get the quantity-selector.
	const itemElement = e.target.closest('.js-cart-item');

	if (!itemElement && itemElement.dataset.lineItemKey) {
		return;
	}

	_updateItem(itemElement.dataset.lineItemKey, parseInt(e.target.value))
		.then(cart => {
			_updateCartHTML(cart)
		});
};
