
export const _updateTotalPrice = (price) => {

	const {
		Shopify,
		theme
 	} = window;

	const elements = document.querySelectorAll('.js-cart-total');

	elements.forEach(element => {
		if (price > 0) {
			element.textContent = Shopify.formatMoney(price, theme.moneyFormat);
		} else {
			element.textContent = '';
		}
	});
};
