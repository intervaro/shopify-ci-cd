
export const _updateCartCount = (count = 0) => {

	const elements = document.querySelectorAll('.js-cart-count');

	elements.forEach(element =>
		element.innerHTML = count
	);
}
