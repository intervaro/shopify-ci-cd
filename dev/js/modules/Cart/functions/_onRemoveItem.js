import { _updateCartHTML } from './_updateCartHTML';
import { _updateItem } from '../../Shopify/functions/_updateItem';

export const _onRemoveItem = (e) => {

	// Get the quantity-selector.
	const itemElement = e.target.closest('.js-cart-item');

	if (!itemElement && itemElement.dataset.lineItemKey) {
		return;
	}

	// Remove item from the cart.
	_updateItem(itemElement.dataset.lineItemKey, 0)
		.then(cart => {
			_updateCartHTML(cart)
		});
};
