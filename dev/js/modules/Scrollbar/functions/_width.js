
/**
 * Set value for '--scrollbar-width' based on the width of the scrollbar.
 */
export const _width = () => {
	const scrollbarWidth = window.innerWidth - document.body.clientWidth;
	document.documentElement.style.setProperty( '--scrollbar-width', `${ scrollbarWidth }px` );
};
