import { Module } from '../Module';
import { _width } from './functions/_width';

/**
 * Module for scrollbar.
 *
 * This module for an example
 * sets the width for '--scrollbar-width'.
 */
export class Scrollbar extends Module {
	initialize() {
		_width();
	}
}
