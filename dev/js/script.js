/* Import polyfills. */
import './functions/polyfills';
import '../../node_modules/objectFitPolyfill/dist/objectFitPolyfill.min.js';

/* :focus-visible polyfill from npm */
import 'focus-visible';

/* Import modules. */
import { Intervaro } from './modules/Intervaro';
import { Modals } from './modules/Modals';
import { Wcag } from './modules/Wcag';
import { Scroll } from './modules/Scroll';
import { Sliders } from './modules/Sliders/';
import { Cookies } from './modules/Cookies';
import { LazyLoad } from './modules/LazyLoad';
import { Accordion } from './modules/Accordion';
import { Scrollbar } from './modules/Scrollbar';
import { Shopify } from './modules/Shopify';
import { Cart } from './modules/Cart';

Intervaro.load( () => {
	new Shopify();
	new Cart();
	new Accordion();
	new Cookies();
	new Modals();
	new Scroll();
	new Scrollbar();
	new Sliders();
} );

Intervaro.ready( () => {
	new LazyLoad();
	new Wcag();
} );
