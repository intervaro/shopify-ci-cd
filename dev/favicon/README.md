# Favicon
Favicons can be generated by running `npm run favicon`. It is using the Node CLI from [RealFavicon](https://realfavicongenerator.net/) with simple default settings that should work for most projects. To modify these settings please change them in `dev/favicon/favicon.json`

## How to
1. Add favicon.svg to `dev/favicon/`
2. Run `npm run favicon`
3. Favicons are automatically generated and put in `dev/favicon/`.
4. Run `npm run watch` or `npm run build` to move favicons to `dist/favicon/`
5. Favicons in `dist/favicon/` are enqueued in `template-parts/head-favicon.php`

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)
