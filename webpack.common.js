/* global __dirname, process, module */

const path = require('path');
const glob = require('glob');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isProduction = 'production' === process.env.NODE_ENV;

// Config files.
const settings = require('./webpack.settings.js');

// Configure entries.
const configureEntries = () => {
	const entries = {};

	for (const [key, value] of Object.entries(settings.entries)) {
		entries[key] = path.resolve(__dirname, value);
	}

	return entries;
};

module.exports = {
	entry: configureEntries(),
	output: {
		path: path.resolve(__dirname, settings.paths.dist.base),
		filename: settings.filename.js,
	},

	// Console stats output.
	// @link https://webpack.js.org/configuration/stats/#stats
	stats: settings.stats,

	// External objects.
	externals: {
		jquery: 'jQuery',
	},

	// Performance settings.
	performance: {
		maxAssetSize: 100000,
	},

	// Build rules to handle asset files.
	module: {
		rules: [

			// Scripts.
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env'],
							cacheDirectory: true,
							sourceMap: !isProduction,
						},
					},
					// Glob
					{
						loader: 'import-glob-loader'
					},
				],
			},

			// Styles.
			{
				test: /\.css$/,
				include: path.resolve(__dirname, settings.paths.src.css),
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					{
						loader: 'css-loader',
						options: {
							sourceMap: !isProduction,
							// We copy fonts etc. using CopyWebpackPlugin.
							url: false,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: !isProduction,
						},
					},
				],
			},
		],
	},

	plugins: [

		// Friendlier errors.
		new FriendlyErrorsWebpackPlugin(),

		// Remove the extra JS files Webpack creates for Sass entries.
		// This should be fixed in Webpack 5.
		new FixStyleOnlyEntriesPlugin({
			silent: true,
		}),

		// Clean the `dist` folder on build.
		new CleanWebpackPlugin({
			cleanStaleWebpackAssets: false,
		}),

		// Extract CSS into individual files.
		new MiniCssExtractPlugin({
			filename: settings.filename.css,
			chunkFilename: '[id].css',
		}),

		// Copy static assets to the `dist` folder.
		new CopyWebpackPlugin([
			{
				from: settings.copyWebpackConfig.fromFonts,
				to: settings.copyWebpackConfig.to,
				context: path.resolve(__dirname, settings.paths.src.base)
			},
			{
				from: settings.copyWebpackConfig.fromImages,
				to: settings.copyWebpackConfig.to,
				context: path.resolve(__dirname, settings.paths.src.base)
			},
			{
				from: settings.copyWebpackConfig.fromFavicon,
				to: settings.copyWebpackConfig.to,
				context: path.resolve(__dirname, settings.paths.src.base)
			},
			{
				from: settings.copyWebpackConfigSVG.from,
				to: settings.copyWebpackConfigSVG.to,
				context: path.resolve(__dirname, settings.paths.src.base)
			},
			{
				from: settings.copyWebpackConfigSVGMulti.from,
				to: settings.copyWebpackConfigSVGMulti.to,
				context: path.resolve(__dirname, settings.paths.src.base)
			},
		]),

		// Minify and optimize image/SVG files.
		new ImageminPlugin({
			test: './**',///\.(jpe?g|png|gif|svg)$/i,
			optipng: {
				optimizationLevel: 7,
			},
			gifsicle: {
				optimizationLevel: 3,
			},
			pngquant: {
				quality: '65-90',
				speed: 4,
			},
			svgo: {
				plugins: [
					{ removeUnknownsAndDefaults: false },
					{ cleanupIDs: true },
					{ removeViewBox: false },
					{
						removeAttrs: {
							attrs: ['data-name', 'id']
						}
					},
					{
						addAttributesToSVGElement: {
							attributes: [
								{ focusable: 'false' },
								{ 'aria-hidden': 'true' },
								{ role: 'img' },
							],
						},
					},
					{
						addClassesToSVGElement: {
							classNames: ['svg'],
						},
					},
				],
			},
			plugins: [imageminMozjpeg({ quality: 75 })],
		}),

		// Minify and optimize SVG monocolor icons files.
		new ImageminPlugin({
			test: '../snippets/**-mono.liquid',
			svgo: {
				plugins: [
					{ removeUnknownsAndDefaults: false },
					{ cleanupIDs: true },
					{ removeViewBox: false },
					{ collapseGroups: true },
					{ removeStyleElement: true },
					{
						removeAttrs: {
							attrs: ['data-name', 'id', 'class', 'style', '(stroke|fill)']
						}
					},
					{
						addAttributesToSVGElement: {
							attributes: [
								{ focusable: 'false' },
								{ 'aria-hidden': 'true' },
								{ role: 'img' },
							],
						},
					},
					{
						addClassesToSVGElement: {
							classNames: ['c-icon'],
						},
					},
				],
			},
		}),

		// Minify and optimize SVG multicolor icons files.
		new ImageminPlugin({
			test: '../snippets/**-multi.liquid',
			svgo: {
				plugins: [
					{ removeUnknownsAndDefaults: false },
					{ cleanupIDs: true },
					{ removeViewBox: false },
					{ collapseGroups: true },
					{ removeStyleElement: true },
					{
						// Converting styles to inline attributes
						inlineStyles: {
							onlyMatchedOnce: false
						}
					},
					{
						removeAttrs: {
							attrs: ['data-name', 'id']
						}
					},
					{
						addAttributesToSVGElement: {
							attributes: [
								{ focusable: 'false' },
								{ 'aria-hidden': 'true' },
								{ role: 'img' },
							],
						},
					},
					{
						addClassesToSVGElement: {
							classNames: ['c-icon', 'c-icon--multi'],
						},
					},
				],
			},
		}),
	],
};
