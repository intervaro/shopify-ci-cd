
# Text styles
bold=$(tput bold)
white=$(tput setaf 7)
lightgreen=$(tput setaf 92)
green=$(tput setaf 2)
txtreset=$(tput sgr0)

echo "${bold}
         :XWWWd.
         :XMWWd.
dxxd;    .;:;;.       .,cdxOOOkxoc'.
NMMWd              .;l0NMMMWWWWMMMNOc.
NMMWd             ;0WWMMNkc;,,;lkNMMWO,
NMMWd            ;KMMMW0;        :KMMM0,
NMMWd           .kMMMMK;          :XMMWx
NMMWd           ,KMMMMk.          .OMMMK
NMMWd           '0MMMMk.          .OMMM0
NMMWd           .xWMMMX:          cNMMWd
NMMWd            ,0WMMMKl.      .lXMMWO'
NMMWd             'xXNMMW0dlcclxKWMMNd.
NMMWd              ..;xKWMMMMMMMMNKd,
kOOO:                  'lk0XNNX0xc.       ${txtreset}"
echo "${bold}
       Create CSS-file
      ${txtreset}"

echo "1) Enter a folder-name (Default: components)"
read folder_name

if test -n "$folder_name"; then
  echo ""
else
  folder_name="components"
fi

echo "2) Enter a type (Default: component)"
read type

if test -n "$type"; then
  echo ""
else
  type="component"
fi

echo "3) Enter a name (Example: Cookie Notice)"
read title

if test -n "$title"; then
  echo ""
else
  exit
fi

# Turn slug into lowercase based on title.
slug=$(echo $title | tr '[:upper:]' '[:lower:]')
type=$(echo $type | tr '[:upper:]' '[:lower:]')

# Replace spaces with dashes in slug.
str_to_replace=" "
replace_str="-"
slug=${slug//$str_to_replace/$replace_str}

str_to_replace="("
replace_str=""
slug=${slug//$str_to_replace/$replace_str}

str_to_replace=")"
replace_str=""
slug=${slug//$str_to_replace/$replace_str}

type_with_s="${type}s"

# Create a formatted titlef for css-header.
title_len="${#title}"
row_len=75
space_count=`expr $row_len - $title_len`
spaces=""

for i in $(eval echo "{1..$space_count}"); do
  spaces+=" "
done

spaces+="#"
title_formatted="${title}${spaces}"

# Get type-letter
type_letter=${type::1}

# Create a components folder in dev/css if it does not already exist.
mkdir -p dev/css/$folder_name

# Create a new css-file for the component.
sed -e "s/PLACEHOLDER_TITLE/$title_formatted/g" bin/code-templates/style.css > dev/css/$folder_name/$slug.css

# Replace type-letter-placeholder.
sed -i '' -e "s/PLACEHOLDER_TYPE_LETTER/$type_letter/g" dev/css/$folder_name/$slug.css

# Replace type-placeholder.
sed -i '' -e "s/PLACEHOLDER_TYPE/$type/g" dev/css/$folder_name/$slug.css

# Replace slug-placeholder.
sed -i '' -e "s/PLACEHOLDER_SLUG/$slug/g" dev/css/$folder_name/$slug.css

echo "CSS-file has been created"

