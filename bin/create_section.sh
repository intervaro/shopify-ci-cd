
# Text styles
bold=$(tput bold)
white=$(tput setaf 7)
lightgreen=$(tput setaf 92)
green=$(tput setaf 2)
txtreset=$(tput sgr0)

echo "${bold}
         :XWWWd.
         :XMWWd.
dxxd;    .;:;;.       .,cdxOOOkxoc'.
NMMWd              .;l0NMMMWWWWMMMNOc.
NMMWd             ;0WWMMNkc;,,;lkNMMWO,
NMMWd            ;KMMMW0;        :KMMM0,
NMMWd           .kMMMMK;          :XMMWx
NMMWd           ,KMMMMk.          .OMMMK
NMMWd           '0MMMMk.          .OMMM0
NMMWd           .xWMMMX:          cNMMWd
NMMWd            ,0WMMMKl.      .lXMMWO'
NMMWd             'xXNMMW0dlcclxKWMMNd.
NMMWd              ..;xKWMMMMMMMMNKd,
kOOO:                  'lk0XNNX0xc.       ${txtreset}"
echo "${bold}
       Create section
      ${txtreset}"

echo "1) Enter a section name (Example: Page Header)"
read title

if test -n "$title"; then
  echo ""
else
  exit
fi

# Turn slug into lowercase based on title.
slug=$(echo $title | tr '[:upper:]' '[:lower:]')
type=$(echo $type | tr '[:upper:]' '[:lower:]')

# Replace spaces with dashes in slug.
str_to_replace=" "
replace_str="-"
slug=${slug//$str_to_replace/$replace_str}

str_to_replace="("
replace_str=""
slug=${slug//$str_to_replace/$replace_str}

str_to_replace=")"
replace_str=""
slug=${slug//$str_to_replace/$replace_str}

# Create a formatted title for the file-header.
title_len="${#title}"
row_len=75
space_count=`expr $row_len - $title_len`
spaces=""

for i in $(eval echo "{1..$space_count}"); do
  spaces+=" "
done

spaces+="#"
title_formatted="${title}${spaces}"

# Get type-letter
type_letter=${type::1}

# Create a sections folder in dev/css if it does not already exist.
mkdir -p dev/css/sections

# Create a new liquid + css-file for the section.
sed -e "s/PLACEHOLDER_TITLE/$title_formatted/g" bin/code-templates/section.liquid > sections/$slug.liquid
sed -e "s/PLACEHOLDER_TITLE/$title_formatted/g" bin/code-templates/section.css > dev/css/sections/$slug.css

# Replace section-title placeholder
sed -i '' -e "s/PLACEHOLDER_SECTION_TITLE/$title/g" sections/$slug.liquid

# Replace slug-placeholder.
sed -i '' -e "s/PLACEHOLDER_SLUG/$slug/g" sections/$slug.liquid
sed -i '' -e "s/PLACEHOLDER_SLUG/$slug/g" dev/css/sections/$slug.css

echo "Section has been created"

