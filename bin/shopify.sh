# Text styles
bold=$(tput bold)
white=$(tput setaf 7)
lightgreen=$(tput setaf 92)
green=$(tput setaf 2)
red=$(tput setaf 160)
txtreset=$(tput sgr0)

echo "${bold}
         :XWWWd.
         :XMWWd.
dxxd;    .;:;;.       .,cdxOOOkxoc'.
NMMWd              .;l0NMMMWWWWMMMNOc.
NMMWd             ;0WWMMNkc;,,;lkNMMWO,
NMMWd            ;KMMMW0;        :KMMM0,
NMMWd           .kMMMMK;          :XMMWx
NMMWd           ,KMMMMk.          .OMMMK
NMMWd           '0MMMMk.          .OMMM0
NMMWd           .xWMMMX:          cNMMWd
NMMWd            ,0WMMMKl.      .lXMMWO'
NMMWd             'xXNMMW0dlcclxKWMMNd.
NMMWd              ..;xKWMMMMMMMMNKd,
kOOO:                  'lk0XNNX0xc.       ${txtreset}"
echo "${bold}${green}
        Shopify
      ${txtreset}"

echo "${red}Make sure to login before running using: npm run login${txtreset}\n"

read -p "Press ENTER to continue"

echo "\nWhat do you want to do?"

PS3="Select an option: "
options=(
	"Start local server" # 1)
	"Push to development" # 2)
	"Push to staging" # 3)
	"Push to production" # 4)ö
	"Upload theme" # 5)
	"List available themes" # 6)
)

select opt in "${options[@]}"
do
	case $opt in
		# 1)
		"Start local server")
			: > .shopifyignore
			sed -e "" bin/code-templates/shopifyignore-minimal.txt > .shopifyignore
			echo "\n${bold}Changed configuration in .shopifyignore to ${green}minimal${txtreset}\n"

			npm run serve

			: > .shopifyignore
			sed -e "" bin/code-templates/shopifyignore-full.txt > .shopifyignore
			echo "\n${bold}Changed configuration in .shopifyignore to ${green}full${txtreset}\n"

			exit
		;;

		# 2)
		"Push to development")
			: > .shopifyignore
			sed -e "" bin/code-templates/shopifyignore-full.txt > .shopifyignore
			echo "\n${bold}Changed configuration in .shopifyignore to ${green}full${txtreset}\n"

			npm run push

			exit
		;;

		# 3)
		"Push to staging")
			: > .shopifyignore
			sed -e "" bin/code-templates/shopifyignore-full.txt > .shopifyignore
			echo "\n${bold}Changed configuration in .shopifyignore to ${green}full${txtreset}\n"

			npm run push:staging

			exit
		;;

		# 4)
		"Push to production")
			: > .shopifyignore
			sed -e "" bin/code-templates/shopifyignore-full.txt > .shopifyignore
			echo "\n${bold}Changed configuration in .shopifyignore to ${green}full${txtreset}\n"

			npm run push:production

			exit
		;;

		# 5)
		"Upload theme")
			: > .shopifyignore
			sed -e "" bin/code-templates/shopifyignore-minimal.txt > .shopifyignore
			echo "\n${bold}Changed configuration in .shopifyignore to ${green}minimal${txtreset}\n"

			npm run push-init

			sed -e "" bin/code-templates/shopifyignore-full.txt > .shopifyignore
			echo "\n${bold}Changed configuration in .shopifyignore to ${green}full${txtreset}\n"

			exit
		;;

		# 6)
		"List available themes")

			shopify theme list

			exit
		;;
		*) echo "invalid option $REPLY";;
	esac
done
