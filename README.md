# Intervaro Start (Shopify)
A repository with a standard starting structure for Shopify projects at Intervaro.

## 1. Setup

### 1.1 Project setup

Project setup is done once in project's lifetime. Do it before modifying anything.

1. Make sure you have installed [Shopify CLI](https://shopify.dev/apps/tools/cli/installation)
1. Run `git clone https://gitlab.com/intervaro/shopify-start.git project-name` to clone theme.
2. Navigate to the theme and run setup wizard in theme root with bash `sh setup.sh`
		1. **Site-url** (Example: sitename.myshopify.com)
		2. **Theme-ID (Development)** for the remote theme.
		3. **Theme-ID (Staging)** for the remote theme.
		4. **Theme-ID (Production)** for the remote theme.
3. Go to "Developer setup"

### 1.2 Developer setup

Every developer does this in the initial setup of the project, it's done once for every developer.

1. Navigate to theme root: `cd ~/Sites/<projectname>`
2. Run `npm install` (fetches all node packgaes for build tools)
3. Go to "Start working"

### 1.3 Start working

Do this everytime you start working with the theme.

1. Navigate to theme root: `cd ~/Sites/<projectname>`
2. Open the project in your text editor!
4. Run `npm run login` to login.
3. Run `npm run watch` to watch assets.
4. Run `npm run serve` to start local-environment.
4. The site is available at [127.0.0.1:9292](http://127.0.0.1:9292/).
5. To quit press `ctrl` + `c`

### 1.3 Deployment

1. In order to deploy run any of the following commands:
		1. Development `npm run push`
		2. Development `npm run push:staging`
		3. Development `npm run push:production`
2. If the theme haven't been pushed to the server before, run `npm run push-init`


### 1.4 Editor Setup

To get Visual Studio Code ready for Intervaro start, we use a workspace configuration including settings and recommended extensions.

#### Setup Visual Studio Code for the first time
1. Navigate to the extensions: `ctrl`/`cmd` + `shift` + `x`.
2. In the search-bar enter: `@recommended`.
3. Install extensions under `Workspace Recommendations`.

#### Add a new extension to the workspace (Optional. Only for specific project)

1. Open the extension that you would like to add.
2. Get the extension ID and add it in: `<projectname>/.vscode/extensions.json`.

_Read more about [workspace-settings](https://code.visualstudio.com/docs/getstarted/settings)_.

![Get Extension-ID](https://code.visualstudio.com/assets/docs/editor/extension-gallery/extension-identifier.png)

## 2. Folder structure
The content is described here.
```
shopify-start/
__dev/
	__fonts/ (font package from https://transfonter.org)
	__images/
	__js/
	__css/
	__svg/
__dist/ (generated assets)
	__css/
	__fonts/
	__images/
	__js/
	__svg/
```

## 3. Shell-scripts

To speed up the development-process, you can use any of the shell-scripts located in `<projectname>/bin`.
To use a script, run it like following in the terminal (from root): `sh bin/[script_name].sh`

Here is a list of all of the available scripts:

| Script             | Description                                                     | Command                                                       |
| :----------------- | :-------------------------------------------------------------- | :------------------------------------------------------------ |
| create_css_file    | Creates a css-file in the specified folder with headers etc.    | `sh bin/create_css_file.sh`                                   |
| create_section     | Creates a section (liquid + css) in /sections with headers etc. | `sh bin/create_section.sh`                                    |
| create_snippet     | Creates a snippet (liquid) in /snippets with headers etc.       | `sh bin/create_snippet.sh`                                    |
