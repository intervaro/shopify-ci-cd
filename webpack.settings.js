/* global module, process */

// Webpack settings exports.
module.exports = {
	entries: {
		// JS files.
		'script': './dev/js/script.js',

		// CSS files.
		'style': './dev/css/style.css',
		//'styleguide-style': './dev/css/styleguide/styleguide.css'
	},
	filename: {
		js: '[name].js',
		css: '[name].css',
	},
	paths: {
		src: {
			base: './dev/',
			css: './dev/css/',
			js: './dev/js/',
		},
		dist: {
			base: './assets/',
			clean: ['./images', './css', './js', './svg', './favicon'],
		}
	},
	stats: {
		// Copied from `'minimal'`.
		all: false,
		errors: true,
		maxModules: 0,
		modules: true,
		warnings: true,
		// Our additional options.
		assets: true,
		errorDetails: true,
		excludeAssets: /\.(jpe?g|png|gif|svg|woff|woff2)$/i,
		moduleTrace: true,
		performance: true,
	},
	vars: {
		cssName: 'styles',
	},
	copyWebpackConfig: {
		fromImages: '../dev/images/*.{jpg,jpeg,png,gif,svg,webp}',
		fromFonts: '../dev/fonts/*.{eot,otf,ttf,woff,woff2}',
		fromFavicon: '../dev/favicon/*.{jpg,jpeg,png,gif,ico,webmanifest,svg,webp,xml}',
		to: '[name].[ext]',
	},
	copyWebpackConfigSVG: {
		from: '../dev/svg/mono/*.svg',
		to: '../snippets/icon-[name]-mono.liquid',
	},
	copyWebpackConfigSVGMulti: {
		from: '../dev/svg/multi/*.svg',
		to: '../snippets/icon-[name]-multi.liquid',
	},
	BrowserSyncConfig: {
		host: 'localhost',
		port: 3000,
		//proxy: 'https://' + storeURL + '?preview_theme_id=' + themeID,
		open: false,
		reloadDelay: 2000,
		middleware: [
			function (req, res, next) {
				// Shopify sites with redirection enabled for custom domains force redirection
				// to that domain. `?_fd=0` prevents that forwarding.
				// ?pb=0 hides the Shopify preview bar
				const prefix = req.url.indexOf('?') > -1 ? '&' : '?';
				const queryStringComponents = ['_fd=0&pb=0'];

				req.url += prefix + queryStringComponents.join('&');
				next();
			}
		],
		files: [
			'**/*.liquid',
			'assets/*.{js,css,jpg,jpeg,png,gif,svg,eot,ttf,woff,woff2}'
		],
	}
};
